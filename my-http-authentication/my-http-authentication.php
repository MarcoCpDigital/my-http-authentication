<?php
/**
 * Plugin Name: My HTTP Authentication
 * Plugin URI: https://marcocp.digital/
 * Description: Plugin to implement HTTP authentication in WordPress
 * Version: 1.0
 * Author: Marco Cp
 * Author URI: https://marcocp.digital/
 */

function my_http_authentication() {
    // Verify user credentials
    $valid_username = 'admin';
    $valid_password = 'password';

    if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])
        || $_SERVER['PHP_AUTH_USER'] !== $valid_username
        || $_SERVER['PHP_AUTH_PW'] !== $valid_password) {
        // Invalid credentials, display error message or redirect the user
        header('WWW-Authenticate: Basic realm="Restricted Access"');
        header('HTTP/1.0 401 Unauthorized');
        echo 'Unauthorized Access';
        exit;
    }
}

add_action('init', 'my_http_authentication');
