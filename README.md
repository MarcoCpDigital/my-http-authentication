# Basic WordPress Plugin

This is a basic WordPress plugin that implements HTTP authentication for restricting access to your WordPress site.

## Installation

1. Download the plugin ZIP file.
2. Extract the contents of the ZIP file to the `wp-content/plugins/` directory of your WordPress installation.
3. Activate the plugin from the WordPress admin dashboard.

## Usage

Once the plugin is activated, it will enforce HTTP authentication for accessing your WordPress site. You can set the valid username and password in the plugin file.

To update the credentials, open the `my-http-authentication.php` file in the plugin folder and modify the values of the `$valid_username` and `$valid_password` variables.

Please note: It is recommended to choose a strong and secure password.

## Customization

You can customize the plugin by modifying the following variables in the `my-http-authentication.php` file:

- `$valid_username` - Set the desired username for authentication.
- `$valid_password` - Set the desired password for authentication.

Make sure to choose a strong password and keep it secure.

## License

This project is licensed under the [MIT License](LICENSE).

## Contributing

Contributions are welcome! If you encounter any issues or have suggestions for improvements, please feel free to [open an issue](https://gitlab.com/MarcoCpDigital/my-http-authentication/-/issues) or submit a pull request.

## Support

If you need any assistance or have questions, please contact [me@marcocp.digital](mailto:me@marcocp.digital).